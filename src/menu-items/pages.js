import { LoginOutlined } from '@ant-design/icons';

const pages = {
    id: 'authentication',
    title: 'Authentication',
    type: 'group',
    children: [
        {
            id: 'login',
            title: 'Login',
            type: 'item',
            url: '/login',
            icon: LoginOutlined,
            target: true
        },        
    ]
};

export default pages;
