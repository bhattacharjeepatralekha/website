import { HomeOutlined, TagsOutlined, FileTextOutlined, HistoryOutlined, SettingOutlined} from '@ant-design/icons';
import BusinessCenterOutlinedIcon from '@mui/icons-material/BusinessCenterOutlined';
import GroupsOutlinedIcon from '@mui/icons-material/GroupsOutlined';

const dashboard = {
    id: 'group-dashboard',
    title: 'Navigation',
    type: 'group',
    children: [
        {
            id: 'dashboard',
            title: 'Dashboard',
            type: 'item',
            url: '/dashboard',
            icon: HomeOutlined,
            breadcrumbs: false
        },
        {
            id: 'tag',
            title: 'Tags',
            type: 'item',
            url: '/tags',
            icon: TagsOutlined,
            breadcrumbs: false
        },
        {
            id: 'documentUpload',
            title: 'Document Upload',
            type: 'item',
            url: '/document-upload',
            icon: FileTextOutlined,
            breadcrumbs: false
        },
        {
            id: 'category',
            title: 'Category',
            type: 'item',
            url: '/document-category',
            icon: BusinessCenterOutlinedIcon,
            breadcrumbs: false
        },
        {
            id: 'history',
            title: 'History',
            type: 'item',
            url: '/history',
            icon: HistoryOutlined,
            breadcrumbs: false
        },
        {
            id: 'group',
            title: 'Group',
            type: 'item',
            url: '/group',
            icon: GroupsOutlinedIcon,
            breadcrumbs: false
        },
        {
            id: 'settings',
            title: 'Settings',
            type: 'item',
            url: '/settings',
            icon: SettingOutlined,
            breadcrumbs: false
        },

    ]
};

export default dashboard;
