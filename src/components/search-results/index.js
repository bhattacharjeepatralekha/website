import React, { useState, useRef, useMemo, useCallback } from "react";
import { AgGridReact } from "ag-grid-react"; // the AG Grid React Component
import { Grid, TextField, MenuItem, Select, Button, Typography, Card, CardHeader, CardContent, Autocomplete } from "@mui/material";
import 'ag-grid-enterprise';
import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-alpine.css"; // Optional theme CSS
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { AiOutlineDownload } from "react-icons/ai";
import { RiDeleteBinLine } from "react-icons/ri";
import { GrView } from "react-icons/gr";
import DocumentSearchDefault from "components/document-search/index";

const dummyGridData = [
    {
        name: "ABC",
        type: "about an hour ago",
        size: "last name, first name",
        lastUpdatedBy: "name of the document",
        lastUpdateDate: "update",
        documentType: "NA"
    },
    {
        name: "BCD",
        type: "about an hour ago",
        size: "last name, first name",
        lastUpdatedBy: "name of the document",
        lastUpdateDate: "update",
        documentType: "NA"
    },
    {
        name: "BCD",
        type: "about an hour ago",
        size: "last name, first name",
        lastUpdatedBy: "name of the document",
        lastUpdateDate: "update",
        documentType: "NA"
    },
    {
        name: "ABC",
        type: "about an hour ago",
        size: "last name, first name",
        lastUpdatedBy: "name of the document",
        lastUpdateDate: "update",
        documentType: "NA"
    },
];

function IconComponent() {
    return (
        <div>
            <AiOutlineDownload size={18} />&nbsp;&nbsp;
            <RiDeleteBinLine size={18} />&nbsp;&nbsp;
            <GrView size={18} />
        </div>
    );
}

const SearchResults = () => {

   // const gridRef = useRef();
   // const [rowData, setRowData] = useState(dummyGridData);

    //const [columnDefs, setColumnDefs] = useState([{ field: "name" }, { field: "type" }, { field: "size" }, { field: "lastUpdatedBy" }, { field: "lastUpdateDate" }, { field: "documentType", filter: true }, { field: "", cellRenderer: "iconComponent" }]);

    const gridRef = useRef();
    const [rowData, setRowData] = React.useState();
    const onGridReady = useCallback(() => {
         setRowData(dummyGridData)
      }, []);

    const [columnDefs, setColumnDefs] = React.useState([
        { field: "name", rowGroup: true },
        { field: "type",  },
        { field: "size" },
        { field: "lastUpdatedBy" },   
        { field: "lastUpdateDate" },
        { field: "documentType", filter: true },
        { field: "", cellRenderer: "iconComponent" }
    ]);
    const defaultColDef = useMemo(() => ({
        sortable: true,
    }));

    const autoGroupColumnDef = {
        headerName: 'Folder',
        minWidth: 220,
        cellRendererParams: {
            suppressCount: true,
            checkbox: true,
        }
    };


    const cellClickedListener = useCallback((event) => {
        console.log("cellClicked", event);
    }, []);


    return (
        <Grid container rowSpacing={10.5} columnSpacing={2.75} sx={{ mt: 1, mb: 1 }}>
            <DocumentSearchDefault />
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1 }}>
                <Card sx={{ width: "100%", height: "100%" }}>
                    <CardHeader title="Search Results" sx={{ paddingLeft: "24px", paddingBottom: "0px" }} />
                    <CardContent sx={{ height: "550px", width: "100%" }}>
                        <div style={{ height: "100%", width: "100%" }} className="ag-theme-alpine">
                            <AgGridReact 
                             rowData={rowData}
                             columnDefs={columnDefs}
                             defaultColDef={defaultColDef}
                             autoGroupColumnDef={autoGroupColumnDef}
                             groupDisplayType={'singleColumn'}
                             animateRows={true}
                             onGridReady={onGridReady}
                              //rowSelection="multiple"
                               onCellClicked={cellClickedListener}
                                frameworkComponents={{
                                    iconComponent: IconComponent
                                }} />
                        </div>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
}
export default SearchResults;