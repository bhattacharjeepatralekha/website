import React,{ useState, useRef, useMemo, useCallback }  from "react";
import { Grid, TextField, MenuItem, Select, Button } from "@mui/material";
import Autocomplete from "@mui/material/Autocomplete";
import SearchResults from "components/search-results";
import { useNavigate } from "react-router-dom";


const documentDummyList = [
    {
        title: "Document 1",
    },
    {
        title: "Document 2",
    },
    {
        title: "Document 3",
    },
];

const DocumentSearchDefault = () => {
    const [categoryType, setCategoryType] = React.useState("All");
    const [documentType, setDocumentType] = React.useState("All");
    const [timeframe, setTimeframe] = React.useState("All");
    const [tag, setTag] = React.useState("All");
    const [isShown, setIsShown] = useState(false);


    const handleChangeCategoryType = (event) => {
        setCategoryType(event.target.value);
    };

    const handleChangeDocumentType = (event) => {
        setDocumentType(event.target.value);
    };

    const handleChangeTimeframe = (event) => {
        setTimeframe(event.target.value);
    };

    const handleChangeTag = (event) => {
        setTag(event.target.value);
    };


    const navigate = useNavigate();
    const Search = event => { 
         navigate("/search-results");
        setIsShown(true);
    };


    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75}>
            <Grid item xs={3} sm={3} md={3} lg={3}>
                <Autocomplete
                    freeSolo
                    id="free-solo-2-demo"
                    size="small"
                    disableClearable
                    options={documentDummyList.map((option) => option.title)}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label="Search Document"
                            InputProps={{
                                ...params.InputProps,
                                type: "search",
                            }}
                        />
                    )}
                />
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <Select
                    labelId="category-type-select-label"
                    id="category-type-select"
                    value={categoryType}
                    sx={{ width: "100%", paddingBottom: "2px" }}
                    size="small"
                    label="Category Type: All"
                    onChange={handleChangeCategoryType}
                >
                    <MenuItem value="All">Category Type: All</MenuItem>
                    <MenuItem value="Internal">Internal</MenuItem>
                    <MenuItem value="Public">Public</MenuItem>
                    <MenuItem value="Partner">Partner</MenuItem>
                    <MenuItem value="Beneficiary">Beneficiary</MenuItem>
                    <MenuItem value="Doctor">Doctor</MenuItem>
                </Select>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <Select
                    labelId="document-type-select-label"
                    id="document-type-select"
                    value={documentType}
                    sx={{ width: "100%", paddingBottom: "2px" }}
                    size="small"
                    label="Document Type: All"
                    onChange={handleChangeDocumentType}
                >
                    <MenuItem value="All">Document Type: All</MenuItem>
                    <MenuItem value="Audit">Audit</MenuItem>
                    <MenuItem value="Legal">Legal</MenuItem>
                    <MenuItem value="Contract">Contract</MenuItem>
                    <MenuItem value="HR">HR</MenuItem>
                    <MenuItem value="Personal Identification">
                        Personal Identification
                    </MenuItem>
                </Select>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <Select
                    labelId="timeframe-select-label"
                    id="timeframe-select"
                    sx={{ width: "100%", paddingBottom: "2px" }}
                    size="small"
                    value={timeframe}
                    label="Timeframe: All"
                    onChange={handleChangeTimeframe}
                >
                    <MenuItem value="All">Timeframe: All</MenuItem>
                    <MenuItem value="-1">Last month</MenuItem>
                    <MenuItem value="-3">Last three month</MenuItem>
                    <MenuItem value="-6">Last six month</MenuItem>
                    <MenuItem value="-12">Older than an year</MenuItem>
                </Select>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <Select
                    labelId="tag-select-label"
                    id="tag-select"
                    value={tag}
                    sx={{ width: "100%", paddingBottom: "2px" }}
                    size="small"
                    label="Tag: All"
                    onChange={handleChangeTag}
                >
                    <MenuItem value="All">Tag: All</MenuItem>
                    <MenuItem value="Confidential">Confidential</MenuItem>
                    <MenuItem value="Unrestricted">Unrestricted</MenuItem>
                    <MenuItem value="Restricted">Restricted</MenuItem>
                </Select>
            </Grid>
            <Grid item xs={1} sm={1} md={1} lg={1}>
                <Button variant="contained" sx={{ width: '100%' }} onClick={Search}>Search</Button>
            </Grid>
        
        </Grid>
    );
};

export default DocumentSearchDefault;
