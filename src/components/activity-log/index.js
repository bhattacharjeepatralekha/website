import React, { useState, useRef, useMemo, useCallback } from "react";
import { AgGridReact } from "ag-grid-react"; // the AG Grid React Component
import { Grid, Card, CardHeader, CardContent } from "@mui/material";
import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-alpine.css"; // Optional theme CSS

const dummyGridData = [
    {
        fileName: "Internal",
        time: "about an hour ago",
        updatedBy: "last name, first name",
        activity: "name of the document",
        action: "update",
    },
    {
        fileName: "Beneficiary",
        time: "about two hours ago",
        updatedBy: "last name, first name",
        activity: "name of the folder",
        action: "create",
    },
    {
        fileName: "Partner",
        time: "a day ago",
        updatedBy: "last name, first name",
        activity: "name of the tag",
        action: "update",
    },
    {
        fileName: "Public",
        time: "5 days ago",
        updatedBy: "last name, first name",
        activity: "name of the tag",
        action: "update",
    },
];
const ActivityLog = () => {
    const gridRef = useRef();
    const [rowData, setRowData] = useState(dummyGridData);

    const [columnDefs, setColumnDefs] = useState([{ field: "fileName" }, { field: "time" }, { field: "updatedBy" }, { field: "activity" }, { field: "action", filter: true }]);

    const defaultColDef = useMemo(() => ({
        sortable: true,
    }));

    const cellClickedListener = useCallback((event) => {
        console.log("cellClicked", event);
    }, []);

    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75} sx={{ mt: 1, mb: 1 }}>
            <Card sx={{ width: "100%", height: "100%" }}>
                <CardHeader title="Activity Log" sx={{ paddingLeft: "24px", paddingBottom: "0px" }} />
                <CardContent sx={{ height: "250px", width: "100%" }}>
                    <div style={{ height: "100%", width: "100%" }} className="ag-theme-alpine">
                        <AgGridReact ref={gridRef} rowData={rowData} columnDefs={columnDefs} defaultColDef={defaultColDef} animateRows={true} rowSelection="multiple" onCellClicked={cellClickedListener} />
                    </div>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default ActivityLog;
