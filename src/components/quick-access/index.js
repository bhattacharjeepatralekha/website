import React from "react";
import { Grid, Typography, List, Stack, Card, CardContent, CardHeader } from "@mui/material";
import Folder from "@mui/icons-material/Folder";

const QuickAccess = () => {
    const FolderView = ({ title, count }) => {
        return (
            <Stack sx={{ alignItems: "center", marginRight: "32px" }}>
                <Typography
                    variant="p"
                    sx={{
                        width: "16px",
                        height: "16px",
                        lineHeight: "16px",
                        borderRadius: "50%",
                        fontSize: "12px",
                        color: "#fff",
                        textAlign: "center",
                        background: "red",
                        position: "absolute",
                        marginLeft: "36px",
                    }}
                >
                    {count}
                </Typography>

                <Folder
                    sx={{
                        color: "#40a9ff",
                        width: "48px",
                        height: "48px",
                    }}
                />
                <Typography variant="p">{title}</Typography>
            </Stack>
        );
    };
    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75} sx={{ mt: 1, mb: 1 }}>
            <Card sx={{ width: "100%" }}>
                <CardHeader title="Quick Access" sx={{paddingLeft: '24px', paddingBottom: '0px'}}/>
                <CardContent>
                    <List component={Stack} direction="row">
                        <FolderView title="Internal" count={23} />
                        <FolderView title="Beneficiary" count={45} />
                        <FolderView title="Partner" count={57} />
                        <FolderView title="Public" count={12} />
                        <FolderView title="Donor" count={43} />
                    </List>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default QuickAccess;