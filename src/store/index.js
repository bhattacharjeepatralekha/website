import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga'
import reducers from './reducers';
import rootSaga from './sagas'


const SagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({ thunk: false }).prepend(SagaMiddleware);
  },
});

SagaMiddleware.run(rootSaga);