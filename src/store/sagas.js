import { all, fork } from "redux-saga/effects";

import QuickAccessDocumentsSaga from "./quick-access-documents/saga";

export default function* rootSaga() {
  yield all([fork(QuickAccessDocumentsSaga)]);
}