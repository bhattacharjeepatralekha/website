import { MENU_ACTIVE_ITEM, MENU_DRAWER_OPEN } from "./actionTypes";

export const setActiveItem = (payload) => {
    return {
        type: MENU_ACTIVE_ITEM,
        payload,
    };
};


export const setDrawerOpen = (payload) => {
    return {
        type: MENU_DRAWER_OPEN,
        payload,
    };
};
