import { MENU_ACTIVE_ITEM, MENU_DRAWER_OPEN } from "./actionTypes";

const initialState = {
    openItem: ["dashboard"],
    drawerOpen: false,
};

const PostReducer = (state = initialState, action) => {
    switch (action.type) {
        case MENU_ACTIVE_ITEM:
            state = { ...state, openItem: action.payload };
            break;
        case MENU_DRAWER_OPEN:
            state = { ...state, drawerOpen: action.payload };
            break;
        default:
            state = { ...state };
            break;
    }
    return state;
};

export default PostReducer;
