import { combineReducers } from "redux";
import QuickAccessReducer from "./quick-access-documents/reducers";
import MenuReducer from "./menu/reducer";

const rootReducer = combineReducers({
  menu: MenuReducer,
  quick_access: QuickAccessReducer
});

export default rootReducer;