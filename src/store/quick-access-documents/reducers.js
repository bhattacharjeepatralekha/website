import {
    GET_DOCUMENTS,
    GET_DOCUMENTS_SUCCESS,
    GET_DOCUMENTS_FAIL,
  } from "./actionTypes";
  
  const initialState = {
    posts: {},
  };
  
  const QuickAccessReducer = (state = initialState, action) => {
    switch (action.type) {
      case GET_DOCUMENTS:
        state = { ...state, loadingPosts: true };
        break;
      case GET_DOCUMENTS_SUCCESS:
        state = { ...state, posts: { data: action.payload, loadingPosts: false, error: false }};
        break;
      case GET_DOCUMENTS_FAIL:
        state = {
          ...state,
          posts: { data: action.payload, loadingPosts: false, error: true },
        };
        break;
      default:
        state = { ...state };
        break;
    }
    return state;
  };
  
  export default QuickAccessReducer;