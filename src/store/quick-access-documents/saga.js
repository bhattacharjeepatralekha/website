import { takeLatest, put, call } from "redux-saga/effects";

import { GET_DOCUMENTS } from "./actionTypes";

import {
  getDocumentsSuccess,
  getDocumentsFail,
} from "./actions";
import { getDocuments } from "restApi/dashboardApi";


function* onGetDocuments() {
  try {
    const response = yield call(getDocuments);
    yield put(getDocumentsSuccess(response));
  } catch (error) {
    yield put(getDocumentsFail(error.response));
  }
}


function* QuickAccessDocumentsSaga() {
  yield takeLatest(GET_DOCUMENTS, onGetDocuments);
}

export default QuickAccessDocumentsSaga;