import { GET_DOCUMENTS, GET_DOCUMENTS_FAIL, GET_DOCUMENTS_SUCCESS } from "./actionTypes";


export const getDocuments = () => {
    return {
        type: GET_DOCUMENTS,
    };
};

export const getDocumentsSuccess = (payload) => {
    return {
        type: GET_DOCUMENTS_SUCCESS,
        payload
    };
};

export const getDocumentsFail = (payload) => {
    return {
        type: GET_DOCUMENTS_FAIL,
        payload
    };
};
