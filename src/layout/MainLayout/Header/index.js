import PropTypes from "prop-types";

import { useTheme } from "@mui/material/styles";
import { IconButton, Toolbar } from "@mui/material";

import AppBarStyled from "./AppBarStyled";
import HeaderContent from "./HeaderContent";

import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";


const Header = ({ open, handleDrawerToggle }) => {
    const theme = useTheme();

    const iconBackColor = "grey.100";
    const iconBackColorOpen = "grey.200";

    const mainHeader = (
        <Toolbar>
            <IconButton
                disableRipple
                aria-label="open drawer"
                onClick={handleDrawerToggle}
                edge="start"
                color="secondary"
                sx={{ color: "text.primary", bgcolor: open ? iconBackColorOpen : iconBackColor, ml: !open ? 6 : 2}}
            >
                {!open ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            </IconButton>
            <HeaderContent />
        </Toolbar>
    );

    const appBar = {
        position: "fixed",
        color: "inherit",
        elevation: 0,
        sx: {
            borderBottom: `1px solid ${theme.palette.divider}`,
        },
    };

    return (
        <AppBarStyled open={open} {...appBar}>
            {mainHeader}
        </AppBarStyled>
    );
};

Header.propTypes = {
    open: PropTypes.bool,
    handleDrawerToggle: PropTypes.func,
};

export default Header;
