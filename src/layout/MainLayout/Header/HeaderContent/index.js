import Profile from './Profile';
import Notification from './Notification'
import {Stack, Typography} from '@mui/material'


const HeaderContent = () => {
    return (
        <Stack sx={{display:'flex', flex: 1, flexDirection: 'row', alignItems: 'center'}}>
            <Stack sx={{alignItems: 'center', flex: 1}}> 
                <Typography variant="h3">DMS-NGO</Typography>
            </Stack>
            <Stack sx={{justifyContent: 'flex-end', flexDirection: 'row',alignItems: 'center'}}> 
                <Notification />
                <Profile />
            </Stack>
        </Stack>
    );
};

export default HeaderContent;
