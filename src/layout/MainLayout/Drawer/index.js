import { useMemo } from "react";
import { Box } from "@mui/material";

import DrawerContent from "./DrawerContent";
import MiniDrawerStyled from "./MiniDrawerStyled";

const MainDrawer = ({ open }) => {
    const drawerContent = useMemo(() => <DrawerContent />, []);

    return (
        <Box component="nav" sx={{ flexShrink: { md: 0 }, zIndex: 1300 }} aria-label="mailbox folders">
            <MiniDrawerStyled variant="permanent" open={open}>
                {drawerContent}
            </MiniDrawerStyled>
        </Box>
    );
};

export default MainDrawer;
