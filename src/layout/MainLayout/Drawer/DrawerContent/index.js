import Navigation from "./Navigation";

const DrawerContent = () => (
    <div>
        <Navigation />
    </div>
);

export default DrawerContent;
