import React from 'react';
import './App.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import Routes from './routes'
import ThemeCustomization from './themes';


function App() {
  return (
    <ThemeCustomization>
      <Routes />
    </ThemeCustomization>
  );
}

export default App;
