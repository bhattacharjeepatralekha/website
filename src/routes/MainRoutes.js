import { lazy } from 'react';

import Loadable from '../components/Loadable';
import MainLayout from '../layout/MainLayout';

const DashboardDefault = Loadable(lazy(() => import('pages/dashboard')));
const TagsDefault = Loadable(lazy(() => import('pages/tags')));
const DocumentUploadDefault = Loadable(lazy(() => import('pages/document-upload')));
const DocumentCategoryDefault = Loadable(lazy(() => import('pages/document-category')));
const HistoryDefault = Loadable(lazy(() => import('pages/history')));
const GroupDefault = Loadable(lazy(() => import('pages/group')));
const SettingsDefault = Loadable(lazy(() => import('pages/settings')));
const SearchResults = Loadable(lazy(() => import('components/search-results')));

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },        
        {
            path: '/dashboard',
            element: <DashboardDefault />            
        },  
        {
            path: '/search-results',
            element: <SearchResults />   
        },
        {
            path: '/tags',
            element: <TagsDefault />            
        },
        {
            path: '/document-upload',
            element: <DocumentUploadDefault />            
        },
        {
            path: '/document-category',
            element: <DocumentCategoryDefault />            
        },
        {
            path: '/history',
            element: <HistoryDefault />            
        },
        {
            path: '/group',
            element: <GroupDefault />            
        },
        {
            path: '/settings',
            element: <SettingsDefault />            
        },   
    ]
};

export default MainRoutes;