import request from './axiosRequest'

export const getDocuments = (id) => {
    return request({
        url:    `users?page=${id}`,
        method: 'GET'
      });
}