import React from "react";
import {Grid, Typography} from '@mui/material';

function SettingsDefault() {

    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75}>
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{mt:1, mb: 1}}>
                <Typography variant="h3">Settings Tab</Typography>
            </Grid>     
        </Grid>
    );
}

export default SettingsDefault;
