import React, {useEffect} from "react";
import { Grid } from "@mui/material";
import ActivityLog from "../../components/activity-log";
import DocumentSearch from "../../components/document-search";
import DocumentUpload from "../../components/document-upload";
import QuickAccess from "../../components/quick-access";
import { useDispatch } from "react-redux";
import { getDocuments } from "store/quick-access-documents/actions";
import SearchResults from "components/search-results";

function DashboardDefault() {
    let dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDocuments());
    }, []);

    return (
        <Grid container rowSpacing={4.5} columnSpacing={2.75}>
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1 }}>
                <DocumentSearch />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1 }}>
                <DocumentUpload />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1, ml: 3 }}>
                <QuickAccess />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} sx={{ mt: 1, mb: 1, ml: 3 }}>
                <ActivityLog />
            </Grid>
        </Grid>
    );
}

export default DashboardDefault;
