import React, { useState } from "react";
import { CardMedia, TextField, Button, Typography, Checkbox, FormControlLabel, Stack, Card, CardContent } from "@mui/material";

function Login() {
    const [isRememberMeChecked, setRememberMeChecked] = useState(false);

    const handleChange = (event) => {
        setRememberMeChecked(event.target.checked);
    };

    return (
        <Stack sx={{ flexDirection: "row", height: "100vh", backgroundColor: "#f5f5f5" }}>
            <Stack sx={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "#005ef7", height: "100%" }}>
                <CardMedia component="img" image={require("../../../assets/login.png")} />
            </Stack>
            <Stack sx={{ flex: 1, justifyContent: "center", alignItems: "center", margin: "2rem 4rem" }}>
                <Card sx={{width: '90%'}}>
                    <CardContent>
                        <Stack sx={{width: '100%', alignItems: "center",}}>
                            <Typography variant="h3">Welcome to DMS-NGO</Typography>
                            <Typography variant="h5" sx={{ alignSelf: "flex-start", margin: "16px" }}>
                                You're almost there!
                            </Typography>
                            <Stack sx={{ margin: "16px 0px" }}>
                                <TextField id="email" label="email" variant="outlined" />
                                <TextField sx={{ margin: "16px 0px" }} id="password" label="password" variant="outlined" />
                            </Stack>
                            <Button variant="contained">Login</Button>
                            <Stack>
                                <FormControlLabel control={<Checkbox checked={isRememberMeChecked} change={handleChange} />} label="Remember me" />
                            </Stack>
                            <Stack sx={{ marginTop: "32px", flexDirection: "row",alignItems: "center", }}>
                                <Button variant="text">Forgot Username?</Button>
                                <Button variant="text">Forgot Password?</Button>
                            </Stack>
                            <Stack sx={{ flexDirection: "row", alignItems: "center", }}>
                                <Typography variant="p">New to DMS-NGO?</Typography>
                                <Button variant="text">Click here to Register</Button>
                            </Stack>
                        </Stack>
                    </CardContent>
                </Card>
            </Stack>
        </Stack>
    );
}

export default Login;
